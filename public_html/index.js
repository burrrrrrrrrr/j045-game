var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/', 'index.html'));
});
http.listen(9700, function () {
});
app.use(express.static('public'));
app.get('/killtheserverbecauseiamtom', function (req, res) {
    res.send("Server Killed.");
    process.exit(1);
});
var userCount = 0;
var totalUser = 0;
var plId = 1;
var boardShape = [];
var masterBoard = [[], []];
var blankBoard = [];
var directionsVar = [[+1, -1, 0], [+1, 0, -1], [0, +1, -1],
    [-1, +1, 0], [-1, 0, +1], [0, -1, +1]];
var playerInfo = [];
var size = 27.75;
var changeLog = [];
var boardSizes = [];
var boardTemp = [];
var itemSw = 0;
var chests = []
var houses = [];
var kickstart = 0;
var lastItem = 'none'
var playerBd = [];
var animationsArr = [];
var noFocus = {_row: 0, _col: 0, _type: 'none'}
var mostBuildings = 0;
var mostVillagers = 0;
var mostOldest = 0;
var namesArr = ['Kory', 'Laverne', 'Bernard', 'Martin', 'Cory', 'Vernon', 'Olen', 'Jesus', 'Hung', 'Micah',
    'Kim', 'Ellis', 'Olin', 'Renato', 'Gino', 'Isidro', 'Francis', 'Cedrick', 'Trent', 'Zack', 'Lucien', 'Reynaldo', 'Christoper', 'Ed',
    'Hank', 'Aaron', 'Rigoberto', 'Melvin', 'Donte', 'Darwin', 'Minh', 'Ethan', 'Willian', 'Bryan', 'Cary', 'Normand', 'Conrad', 'Antonio',
    'Christopher', 'Jospeh', 'Emile', 'Orlando', 'Jerome', 'Curtis', 'Theodore', 'Ignacio', 'Mikel', 'Dennis', 'Rayford', 'Ariel',
    'Luanne', 'Carolyn', 'Jillian', 'Pennie', 'Latonya', 'Romona', 'Alline', 'Jasmin', 'Gloria', 'Ava', 'Camellia', 'Fallon', 'Karoline',
    'Melaine', 'Sage', 'Ludie', 'Yer', 'Rema', 'Shirely', 'Afton', 'Trish', 'Soon', 'Lin', 'Katelyn', 'Lynda', 'Myrle', 'Larisa', 'Julienne',
    'Shayna', 'Noelia', 'Vickie', 'Ressie', 'Soo', 'Peggie', 'Candy', 'Kazuko', 'Valrie', 'Stefany', 'Meaghan', 'Olene', 'Valencia',
    'Talisha', 'Catherina', 'Avis', 'Melynda', 'Crissy', 'Jonnie', 'Brenna', 'Erma', 'Tomi']
var vilStats = {
    _move: 2,
    _attack: 1,
    _attackDmg: 1,
    _hp: 3,
    _hpMax: 3,
    _build: 10,
    _demolish: 10,
    _range: 1,
    _harvest: 4,
    _moveanddo: 0,
    _age: 0,
    _none: 0,
    _refocusVil: 0,
    _refocusBuilding: 0
}

var itemArr = ['npc', 'npc',
    'block', 'block', 'block', 'block', 'block', 'block',
    'block', 'block', 'block', 'block', 'block', 'block',
    'stone', 'stone', 'stone','stone', 'stone', 'stone',
    'sheep', 'sheep',
    'wood', 'wood', 'wood', 'wood', 'wood', 'wood', 'wood',
    'wood', 'wood', 'wood', 'wood', 'wood', 'wood', 'wood', 'wood',
    'wood', 'wood', 'wood', 'wood', 'wood', 'wood', 'wood',
    'wood', 'wood', 'wood', 'wood', 'wood', 'wood', 'wood',
    'none', 'none', 'none', 'none', 'none', 'none',
    'none', 'none', 'none', 'none', 'none', 'none'
];

var stuffInfo = {
    menu: {// just to trick some functions
        image: "style/art/wall.png",
        x: 0,
        y: 8,
        z: 5000,
        wood: 0,
        stone: 0},
    none: {// just to trick some functions
        image: "style/art/wall.png",
        x: 0,
        y: 8,
        z: 5000,
        wood: '-',
        stone: '-'},
    home: {// just to trick some functions
        image: "style/art/homesel.png",
        x: -10,
        y: 19,
        z: 5000,
        wood: '-',
        stone: '-'},
    vil: {
        image: "style/art/man.png",
        x: -4,
        y: 10,
        z: 9000,
        sight: 3,
        wood: 0,
        stone: 0},
    vilAlt: {
        image: "style/art/man2.png",
        x: -2,
        y: 8,
        z: 9000,
        wood: 0,
        stone: 0},
    chest: {
        image: "style/art/chest.png",
        x: 0,
        y: 30,
        z: 1000,
        sight: 2,
        title: 'Paths',
        desc: 'Only your villagers can walk on these paths.',
        wood: 0,
        stone: 0},
    wall: {
        image: "style/art/wall.png",
        x: -4,
        y: 2,
        z: 5001,
        sight: 2,
        title: 'Paths',
        desc: 'Only your villagers can walk on these paths.',
        wood: 10,
        stone: 0},
    house: {
        image: "style/art/house.png",
        x: 0,
        y: -15,
        z: 5002,
        sight: 2,
        title: 'House',
        desc: 'If your villager dies they will go back to thier house. Each villager can have only one house.',
        wood: 50,
        stone: 0},
    watchtower: {
        image: "style/art/watchtower.png",
        x: 12,
        y: -8,
        z: 5003,
        sight: 7,
        title: 'Watchtower',
        desc: 'Can see far into the distance. Does not attack.',
        wood: 50,
        stone: 10},
    guardtower: {
        image: "style/art/guardtower.png",
        x: 0,
        y: -4,
        z: 5004,
        sight: 3,
        wood: 70,
        stone: 20,
        title: 'Guardtower',
        desc: 'Shoots at nearby villagers. Must be manually targeted & can attack every 30 seconds.',
        _attack: 10,
        _attackDmg: 1,
        _range: 5},
    trebuchet: {
        image: "style/art/trebuchet.png",
        x: -2,
        y: 15,
        z: 5005,
        sight: 3,
        wood: 100,
        stone: 40,
        title: 'Trebuchet',
        desc: 'Knocks down enemy buildings from afar. Must be manually targeted & can attack every 60 seconds.',
        _attack: 60,
        _attackDmg: 0,
        _range: 10},
    sheep: {
        image: "style/art/sheep.png",
        x: -5,
        y: 5,
        z: 6004,
        wood: 0,
        stone: 0, },
    npc: {
        image: "style/art/npc.png",
        x: 4,
        y: 0,
        z: 6000,
        wood: 0,
        stone: 0},
    block: {
        image: "style/art/mountain.png",
        x: -8,
        y: 0,
        z: 6003,
        wood: 0,
        stone: 0},
    wood: {
        image: "style/art/tree.png",
        x: 0,
        y: 4,
        z: 6002,
        wood: 0,
        stone: 0},
    stone: {
        image: "style/art/ore.png",
        x: 0,
        y: 12,
        z: 6001,
        wood: 0,
        stone: 0},
    focus1: {
        title: "focus1",
        x: -2,
        y: 45,
        z: 30000,
        wood: 0,
        stone: 0},
    focus2: {
        title: "focus2",
        x: -2,
        y: 49,
        z: 30000,
        wood: 0,
        stone: 0},
    focus3: {
        image: "style/art/focus3.png",
        x: -2,
        y: 0,
        z: 30000,
        wood: 0,
        stone: 0},
    focus4: {
        image: "style/art/focus4.png",
        x: -1,
        y: 0,
        z: 30000,
        wood: 0,
        stone: 0},
    upgrades: {
        menu: {// just to trick some functions
            image: "style/art/wall.png",
            x: 0,
            y: 8,
            z: 5000,
            wood: 0,
            stone: 0,
            sheep: 0},
        move: {
            image: "style/art/foot.png",
            sheep: 10,
            wood: 20,
            limit: 1,
            title: 'Shoes',
            titleStats: 'Movement',
            desc: 'Lets you run faster'
        },
        harvest: {
            image: "style/art/tool.png",
            sheep: 5,
            wood: 20,
            limit: 1,
            title: 'Tools',
            titleStats: 'Harvesting',
            desc: 'Can harvest resources & demolish buildings at a quicker rate'
        },
        demolish: {
            image: "style/art/tool.png",
            sheep: 0,
            wood: 0,
            limit: 1,
            title: 'Demolishing',
            titleStats: 'Demolishing',
            desc: '-'
        },
        attack: {
            image: "style/art/sword.png",
            sheep: 0,
            wood: 0,
            x: 5,
            y: 10,
            desc: 'Move faster'
        },
        attackDmg: {
            image: "style/art/sword.png",
            sheep: 3,
            wood: 20,
            limit: 4,
            title: 'Weapons',
            titleStats: 'Weapon damage',
            desc: 'Increases attack damage.'
        },
        range: {
            image: "style/art/bow.png",
            sheep: 5,
            wood: 20,
            limit: 3,
            title: 'Ranged Weaponry',
            titleStats: 'Weapon range',
            desc: 'Grants increased range of attack'
        },
        hp: {
            image: "style/art/jacket.png",
            sheep: 3,
            wood: 0,
            limit: 15,
            title: 'Health',
            titleStats: 'Current health',
            desc: 'Move faster'
        },
        hpMax: {
            image: "style/art/jacket.png",
            sheep: 5,
            wood: 20,
            limit: 15,
            title: 'Clothing',
            titleStats: 'Max health',
            desc: 'Increases your maximum health'
        },
    },
    colors: {
        colors: ["#801A00", "#FF0000", "#FF7519", "#FFFF00", "#008A00", "#86DDDD", "#0033CC", "660099",
            "#FF80B5", "#A37ACC", "#FF00FF", "#000000", "#606060", "#B8B8B8", "#FFFFFF"],
        users: []
    }
}


function Start() {
    for (it = 0; it < 100; it++) {
        nit = it;
        if (it < 5) {
            nit = 5;
        }
        boardSizes.push(BoardSizeCalc(nit));
    }
    masterBoard = BoardSizer(1, boardSizes[1].height, boardSizes[1].width)
}

function BoardSizeCalc(x) { // (x) being player count
    var i = 0;
    var kount = 192; //spaces per player
    var constant = 24;
    var nonConstant;
    var sw = 0;
    while (i < x) {
        nonConstant = x * kount / constant;
        if (nonConstant >= constant * 2) {
            constant = constant * 2;
        }
        i++;
    }
    nonConstant = x * kount / constant;
    nonConstant = 2 * Math.round(nonConstant / 2);
    var area = constant * nonConstant;
    var space = area / kount / x;
    if (constant > nonConstant) {
        sw = nonConstant;
        nonConstant = constant;
        constant = sw;
    }
    return {height: constant, width: nonConstant, area: area};
}

function BoardSizer(plCount, height, width) {
    boardTemp = [];
    boardTemp = BoardGenerator(height, width);
    for (i = 0; i < boardTemp.length; i++) {
        for (j = 0; j < boardTemp[i].length; j++) {
            boardTemp[i][j].edge = 0;
            if (j === 0 || j === boardTemp[i].length - 1) {
                boardTemp[i][j].edge = 1;
            }
            if (i === 0 || i === boardTemp.length - 1) {
                boardTemp[i][j].edge = 2;
            }
            boardTemp[i][j].i = i;
            boardTemp[i][j].j = j;
            boardTemp[i][j].cube = GridToCube({row: i, col: j});
            boardTemp[i][j].item = ItemGenerator(itemArr);
            boardTemp[i][j].pop = {};
            boardTemp[i][j].building = {};
            boardTemp[i][j].directions =
                    Directions(boardTemp[i][j].cube, boardTemp[i][j].edge, plCount,
                            boardTemp.length, boardTemp[i].length);
        }
    }
    return boardTemp
}

function BoardGenerator(height, width) {
    var table = new Array(height);
    for (i = 0; i < table.length; i++) {
        table[i] = new Array(width);
        for (j = 0; j < table[i].length; j++) {
            table[i][j] = {};
        }
    }
    return table;
}

function BoardReplace(plCount) {
    var table = BoardSizer(plCount, boardSizes[plCount].height, boardSizes[plCount].width)
    for (i = 0; i < table.length; i++) {
        for (j = 0; j < table[i].length; j++) {
            if (i < masterBoard.length && j < masterBoard[0].length) {
                table[i][j].pop = masterBoard[i][j].pop;
                table[i][j].building = masterBoard[i][j].building;
                table[i][j].item = masterBoard[i][j].item;
            }
        }
    }
    return table
}

function SheepMover() {
    setInterval(function () {
        var reproduce = 0;
        var area = boardSizes[totalUser].area * 0.01;
        var newTrees = [];
        var reSheep = [];
        var deSheep = [];
        var data = []
        for (i = 0; i < area; i++) {
            var is = Math.floor((Math.random() * boardSizes[totalUser].height) + 0);
            var js = Math.floor((Math.random() * boardSizes[totalUser].width) + 0);
            if (VacancyTest({row: is, col: js}, 'none') === true) {
                newTrees.push({row: is, col: js});
            }
        }
        for (j = 0; j < newTrees.length; j++) {
            masterBoard[newTrees[j].row][newTrees[j].col].item = 'wood';
        }
        area = boardSizes[totalUser].area * 0.02
        for (i = 0; i < area; i++) {
            var is = Math.floor((Math.random() * boardSizes[totalUser].height) + 0);
            var js = Math.floor((Math.random() * boardSizes[totalUser].width) + 0);
            if (VacancyTest({row: is, col: js}, 'none') === 'sheep') {
                for (ks = 0; ks < 5; ks++) {
                    var coords = CubeToGrid(masterBoard[is][js].directions[ks])
                    var vac = VacancyTest(coords, 'none')
                    if (vac === 'sheep') {
                        reproduce = 1;
                    }
                    if (vac === true) {
                        if (reproduce === 0) {
                            masterBoard[is][js].item = 'none'
                            deSheep.push({row: is, col: js})
                        }
                        masterBoard[coords.row][coords.col].item = 'sheep'
                        reSheep.push(coords)
                        break;
                    }
                }
            }
        }
        data.push(newTrees, reSheep, deSheep)
        Animations('none', 'none', 'items', data)
    }, 10000)
}

function RandomHex() {
    var rw = Math.floor((Math.random() * boardShape.height) + 0);
    var cl = Math.floor((Math.random() * boardShape.width) + 0);
    return {row: rw, col: cl};
}

function ItemGenerator(arr) {
    var n = Math.floor((Math.random() * (arr.length)) + 0)
    if (n < arr.length) {
        if (lastItem !== 'npc' && lastItem !== 'none' &&
                Math.floor((Math.random() * 10) + 1) > 7) {
            return lastItem
        }
        lastItem = arr[n];
        return arr[n];
    }

}

//# convert cube to odd-q offset
function CubeToGrid(cube) {
    var col = cube.x;
    var row = cube.z + (cube.x - (cube.x & 1)) / 2;
    return {row: row, col: col};
}

// convert odd-q offset to cube
function GridToCube(coords) {
    var cube = {};
    cube.x = coords.col;
    cube.z = coords.row - (coords.col - (coords.col & 1)) / 2;
    cube.y = -cube.x - cube.z;
    return cube;
}

function PixelToHex(x, y) {
    var h = {};
    h.q = x * 2 / 3 / size;
    h.r = (-x / 3 + Math.sqrt(3) / 3 * y) / size;
    var hex = HexToCube(h);
    return hex;
}

function CubeRound(h) {
    var rx = Math.round(h.x);
    var ry = Math.round(h.y);
    var rz = Math.round(h.z);
    var x_diff = Math.abs(rx - h.x);
    var y_diff = Math.abs(ry - h.y);
    var z_diff = Math.abs(rz - h.z);
    if (x_diff > y_diff && x_diff > z_diff) {
        rx = -ry - rz;
    } else if (y_diff > z_diff) {
        ry = -rx - rz;
    } else {
        rz = -rx - ry;
    }
    ;
    return {x: rx, z: rz, y: ry};
}

function HexToCube(h) {
    var he = {};
    he.x = h.q;
    he.z = h.r;
    he.y = -he.x - he.z;
    var hexInt = CubeRound(he);
    return hexInt;
}


function CubeToAxial(h) { // axial
    var ax = {};
    ax.q = h.x;
    ax.r = h.z;
    return ax;
}

function AxialToCube(ax) { // axial
    var cube = {};
    cube.x = ax.q;
    cube.z = ax.r;
    cube.y = -ax.q - ax.r;
    return cube;
}

function OffEdgeOfGrid(coords, height, width) {
    var adjusted = {}
    adjusted.row = coords.row;
    adjusted.col = coords.col;
    if (coords.row < 0) {
        adjusted.row = coords.row + height;
    }
    if (coords.row > height - 1) {
        adjusted.row = coords.row - height;
    }
    var colVar = 0;
    colVar += coords.col
    if (coords.col < 0) {
        adjusted.col = colVar + width
    }
    if (coords.col >= width) {
        if (colVar & 1 !== 1) {
            adjusted.row += 1
        }
        adjusted.col = colVar - width
    }
    return adjusted;
}

function CubeDistance(a, b) { //distance between points a and b   
    var dist = (Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z)) / 2;
    var aC = CubeToGrid(a)
    var bC = CubeToGrid(b)
    var rowDist = Math.abs(aC.row - bC.row)
    var colDist = Math.abs(aC.col - bC.col);
    if (rowDist >= boardSizes[totalUser].height / 2) {
        if (aC.row > bC.row) {
            aC.row -= boardSizes[totalUser].height;
        } else {
            bC.row -= boardSizes[totalUser].height;
        }
    }
    if (colDist >= boardSizes[totalUser].width / 2) {
        if (aC.col > bC.col) {
            aC.col -= boardSizes[totalUser].width;
        } else {
            bC.col -= boardSizes[totalUser].width;
        }
    }
    a = GridToCube(aC);
    b = GridToCube(bC);
    var dist = (Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z)) / 2;
    return dist
}

function Directions(cube, edger, plCount, height, width) {
    var dirs = [];
    for (rt = 0; rt < directionsVar.length; rt++) {
        dirs.push({x: cube.x + directionsVar[rt][0],
            z: cube.z + directionsVar[rt][1], y: cube.y + directionsVar[rt][2]});
        if (edger > 0) {
            dirs[rt] = GridToCube(OffEdgeOfGrid(CubeToGrid(dirs[rt]), height, width));
        }
    }
    return dirs;
}

function RandStart(users) {
    var coords = {};
    for (i = 2; i < 100; i + 2) {
        coords.row = (Math.floor((Math.random() * (boardSizes[users].height / 2) - 2) + 2)) * 2
        coords.col = (Math.floor((Math.random() * (boardSizes[users].width / 2) - 2) + 2)) * 2
        OffEdgeOfGrid(coords, boardSizes[users].height, boardSizes[users].width)
        if (VacancyTest(coords) === 'wood')
        {
            masterBoard[coords.row][coords.col].item = 'none'
            break
        }
    }
    return coords;
}

function VillagerNamer(){
    return namesArr[Math.floor((Math.random() * 100) + 0)];
}

function AddPlayer(pl) {
    var rand = RandStart(totalUser);
    playerInfo.push({id: pl, view: {row: 0, col: 0}, viewSize: {width: 22, height: 16}, center: {width: 0, height: 0},
        focus: {id: "none", row: 0, col: 0}, text: {name: '', moto: ''}, int: 0, vils: [], buildings: [], res: {wood: 0, stone: 0, sheep: 0}});
    var viewSet = rand;
    plInt = PlayerInfoSearch(pl);
    stuffInfo.colors.users.push({id: playerInfo[plInt].id, colors: ['', '']})
    playerInfo[plInt].int = plInt
}

function PlayerJoin(plInt) {
    var rand = RandStart(totalUser);
    playerInfo[plInt].view.row = rand.row;
    playerInfo[plInt].view.col = rand.col;
    playerInfo[plInt].int = plInt
    playerInfo[plInt].view = Compass({axis: 'n'}, plInt);
    playerInfo[plInt].view = Compass({axis: 'n'}, plInt);
    playerInfo[plInt].view = Compass({axis: 'w'}, plInt);
    playerInfo[plInt].view = Compass({axis: 'w'}, plInt);
    playerInfo[plInt].center = rand;
    var vilSpawn = new NewStuff('vil', rand.row, rand.col, playerInfo[plInt].id, 0, IDMaker(), BdAppend, Focus, Move, [], {}, {}, Harvest,
            VillagerNamer(), 0, 0, 0, Attack, Build, Upgrade, Trade, Demolish, 0, JSON.parse(JSON.stringify(vilStats)));
    vilSpawn._bdAppend();
    vilSpawn._focus(playerInfo[plInt].id);
    playerInfo[plInt].vils.push(vilSpawn);
    TextHelp([0, 13], 'all', playerInfo[plInt].text.name);
}

function PlayerInfoSearch(id) {
    for (hh = 0; hh < playerInfo.length; hh++) {
        if (playerInfo[hh].id === id) {
            return hh;
        }
    }
}

function VillagerSearch(id, plInt) {
    for (hh = 0; hh < playerInfo[plInt].vils.length; hh++) {
        if (playerInfo[plInt].vils[hh]._id === id) {
            return hh;
        }
    }
}

function Stuff(type, row, col, owner, active, id, BdAppend, Focus, Move, moveQ, secDest, attTarget, Harvest,
        name, wood, stone, sheep, Attack, Build, Upgrade, Trade, Demolish, state, stats) {
    this._type = type;
    this._row = row;
    this._col = col;
    this._owner = owner;
    this._active = active;
    this._id = id;
    this._bdAppend = BdAppend;
    this._focus = Focus;
    this._move = Move;
    this._moveQ = moveQ;
    this._secDest = secDest;
    this._attTarget = attTarget;
    this._harvest = Harvest;
    this._name = name;
    this._wood = wood;
    this._stone = stone;
    this._sheep = sheep;
    this._attack = Attack;
    this._build = Build;
    this._upgrade = Upgrade;
    this._trade = Trade;
    this._demolish = Demolish;
    this._state = state;
    this._stats = stats;
}

Stuff.prototype.set = function fn1() {
    return this._type;
    return this._row;
    return this._col;
    return this._owner;
    return this._active;
    return this._id;
    return this._bdAppend;
    return this._focus;
    return this._move;
    return this._moveQ;
    return this._secDest;
    return this._attTarget;
    return this._harvest;
    return this._name;
    return this._wood;
    return this._stone;
    return this._sheep;
    return this._attack;
    return this._build;
    return this._upgrade;
    return this._trade;
    return this._demolish;
    return this._state;
    return this._stats;
};

function NewStuff(type, row, col, owner, active, id, BdAppend, Focus, Move, moveQ, secDest, attTarget, Harvest,
        name, wood, stone, sheep, Attack, Build, Upgrade, Trade, Demolish, state, stats) {
    Stuff.call(this, type, row, col, owner, active, id, BdAppend, Focus, Move, moveQ, secDest, attTarget, Harvest,
            name, wood, stone, sheep, Attack, Build, Upgrade, Trade, Demolish, state, stats);
}

NewStuff.prototype =
        Object.create(Stuff.prototype);
NewStuff.prototype.constructor = NewStuff;
NewStuff.prototype.set = function fn2() {
    return Stuff.prototype.set.call(this);
};

function VilBuildingArrRemover(stuff) {
    if (stuff._type === 'vil') {
        for (i = 0; i < playerInfo[PlayerInfoSearch(stuff._owner)].vils.length; i++) {
            if (stuff._id === playerInfo[PlayerInfoSearch(stuff._owner)].vils[i]._id) {
                playerInfo[PlayerInfoSearch(stuff._owner)].vils.splice(i, 1)
            }
        }
    } else {
        for (i = 0; i < playerInfo[PlayerInfoSearch(stuff._owner)].buildings.length; i++) {
            if (stuff._id === playerInfo[PlayerInfoSearch(stuff._owner)].buildings[i]._id) {
                playerInfo[PlayerInfoSearch(stuff._owner)].buildings.splice(i, 1)
            }
        }
    }
}

function BdAppend() {
    masterBoard[this._row][this._col].pop = this;
}

function Focus(player) {
    playerInfo[PlayerInfoSearch(player)].focus = this
}

function Harvest(destination) {
    if (masterBoard[destination.row][destination.col].item === 'wood') {
        playerInfo[PlayerInfoSearch(this._owner)].res.wood += 10;
        Animations({row: this._row, col: this._col}, destination, 'harvest', 'none')
        masterBoard[destination.row][destination.col].item = 'none';
        TextHelp([0, 17], this._owner, '', {row: this._row, col: this._col}, this._id);
    }
    if (masterBoard[destination.row][destination.col].item === 'stone') {
        playerInfo[PlayerInfoSearch(this._owner)].res.stone += 10;
        Animations({row: this._row, col: this._col}, destination, 'harvest', 'none')
        masterBoard[destination.row][destination.col].item = 'none';
        TextHelp([0, 18], this._owner, '', {row: this._row, col: this._col}, this._id);
    }
    if (masterBoard[destination.row][destination.col].item === 'sheep') {
        playerInfo[PlayerInfoSearch(this._owner)].res.sheep += 1;
        this._stats._hp = this._stats._hpMax;
        Animations({row: this._row, col: this._col}, destination, 'harvest', 'none')
        masterBoard[destination.row][destination.col].item = 'none';
        TextHelp([0, 19], this._owner, '', {row: this._row, col: this._col}, this._id);
    }
    if (masterBoard[destination.row][destination.col].item === 'chest') {
        var ijer = 0;
        for (ij = 0; ij < chests.length; ij++) {
            if (destination.row === chests[ij].row && destination.col === chests[ij].col) {
                playerInfo[PlayerInfoSearch(this._owner)].res.wood += chests[ij].wood;
                playerInfo[PlayerInfoSearch(this._owner)].res.stone += chests[ij].stone;
                playerInfo[PlayerInfoSearch(this._owner)].res.sheep += chests[ij].sheep;
                TextHelp([0, 11], this._owner, chests[ij].wood + ' wood, ' + chests[ij].stone + ' stone & ' + chests[ij].sheep + ' sheep',
                        {row: chests[ij].row, col: chests[ij].col}, this._id);
                Animations({row: this._row, col: this._col}, destination, 'harvest', 'none');
                masterBoard[destination.row][destination.col].item = 'none';
                chests.splice(ij, 1);
            }
        }
    }
    if (masterBoard[destination.row][destination.col].item === 'npc' && playerInfo[PlayerInfoSearch(this._owner)].res.wood >= stuffInfo.house.wood &&
            playerInfo[PlayerInfoSearch(this._owner)].vils.length < 12) {
        masterBoard[destination.row][destination.col].item = 'none';
        Animations({row: this._row, col: this._col}, destination, 'harvest', 'none');
        playerInfo[PlayerInfoSearch(this._owner)].res.wood -= stuffInfo.house.wood;
        var vilSpawn = new NewStuff('vil', destination.row, destination.col, this._owner, 0, IDMaker(), BdAppend, Focus, Move, [], {}, {}, Harvest,
                VillagerNamer(), stuffInfo.house.wood, 0, 0, Attack, Build, Upgrade, Trade, Demolish, 0, JSON.parse(JSON.stringify(vilStats)));
        vilSpawn._bdAppend();
        playerInfo[PlayerInfoSearch(this._owner)].vils.push(vilSpawn)
        TextHelp([0, 12], this._owner, '', destination);
        Animations({row: vilSpawn._row, col: vilSpawn._col}, 'none', 'move', vilSpawn)
    }
    if (masterBoard[destination.row][destination.col].item === 'npc' && playerInfo[PlayerInfoSearch(this._owner)].res.wood < stuffInfo.house.wood) {
        TextHelp([0, 15], this._owner, '', destination);
    }
    if (masterBoard[destination.row][destination.col].item === 'npc' &&
            playerInfo[PlayerInfoSearch(this._owner)].vils.length >= 12) {
        TextHelp([0, 16], this._owner, '', destination);
    }
}

function Move(destination) {
    masterBoard[destination.row][destination.col].pop = this;
    masterBoard[this._row][this._col].pop = {};
    this._row = destination.row;
    this._col = destination.col;
    Animations({row: this._row, col: this._col}, 'none', 'move', this)
}

function Demolish(coords) {
    TextHelp([0, 8], masterBoard[coords.row][coords.col].building._owner,
            playerInfo[PlayerInfoSearch(masterBoard[coords.row][coords.col].building._owner)].text.name, coords)
    Animations({row: this._row, col: this._col}, coords, 'demolish', masterBoard[coords.row][coords.col].building._id)
    if (masterBoard[coords.row][coords.col].building._type === 'house') {
        for (i = 0; i < houses.length; i++) {
            if (houses[i].house === masterBoard[coords.row][coords.col].building._id) {
                houses.splice(i, 1)
            }
        }
    }
    playerInfo[PlayerInfoSearch(this._owner)].res.wood += Math.round(stuffInfo[masterBoard[coords.row][coords.col].building._type].wood / 2);
    playerInfo[PlayerInfoSearch(this._owner)].res.stone += stuffInfo[masterBoard[coords.row][coords.col].building._type].stone;
    VilBuildingArrRemover(masterBoard[coords.row][coords.col].building)
    masterBoard[coords.row][coords.col].building = {};
}

function Trade(droppings) {
    var droppage = droppings;
    if (droppage[0] + droppage[1] + droppage[2] > 0 && masterBoard[this._row][this._col].item === 'none') {
        masterBoard[this._row][this._col].item = 'chest';
        chests.push({row: this._row, col: this._col, wood: droppage[0], stone: droppage[1], sheep: droppage[2]})
        Animations({row: this._row, col: this._col}, 'none', 'move', {_type: 'chest', _id: this._row + 'i' + this._col})
    }
}

function Attack(destination, dist, plArr) {
    var bdDest = masterBoard[destination.row][destination.col].pop;
    this._attTarget = {pl: bdDest._owner, vil: bdDest._id};
    if (this._stats._range >= dist && this._active === 0) {
        bdDest._stats._hp -= this._stats._attackDmg;
        TextHelp([0, 2], bdDest._owner, bdDest._name, destination, bdDest._id);
        if (bdDest._stats._hp < 1) {
            this._attTarget = {};
            this._moveQ = [];
            bdDest._trade([Math.round(playerInfo[PlayerInfoSearch(bdDest._owner)].res.wood / 10),
                Math.round(playerInfo[PlayerInfoSearch(bdDest._owner)].res.stone / 10),
                Math.max(Math.round(playerInfo[PlayerInfoSearch(bdDest._owner)].res.sheep / 10), 1)])
            playerInfo[PlayerInfoSearch(bdDest._owner)].res.wood -= Math.round(playerInfo[PlayerInfoSearch(bdDest._owner)].res.wood / 10)
            playerInfo[PlayerInfoSearch(bdDest._owner)].res.stone -= Math.round(playerInfo[PlayerInfoSearch(bdDest._owner)].res.stone / 10)
            playerInfo[PlayerInfoSearch(bdDest._owner)].res.sheep -= Math.max(Math.round(playerInfo[PlayerInfoSearch(bdDest._owner)].res.sheep / 10), 1)
            var hasHouse = 0;
            for (i = 0; i < houses.length; i++) {
                if (bdDest._id === houses[i].vil) {
                    hasHouse = 1;
                    bdDest._stats._hp = bdDest._stats._hpMax;
                    if (masterBoard[houses[i].row][houses[i].col].pop._id !== undefined) {
                        for (j = 0; j < 6; j++) {
                            var gridDir = CubeToGrid(masterBoard[houses[i].row][houses[i].col].directions[j]);
                            if (VacancyTest(gridDir, bdDest._owner) === true) {
                                masterBoard[houses[i].row][houses[i].col].pop._move(gridDir);
                                break;
                            }
                            if (j === 5) {
                                for (k = 0; k < 6; k++) {
                                    var gridDir = CubeToGrid(masterBoard[houses[i].row][houses[i].col].directions[k]);
                                    if (masterBoard[gridDir.row][gridDirs.col].pop._id === undefined) {
                                        masterBoard[houses[i].row][houses[i].col].pop._move(gridDir);
                                        break;
                                    }
                                    if (k === 5) {
                                        bdDest.pop = {}
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    bdDest._move({row: houses[i].row, col: houses[i].col});
                    Rester({row: houses[i].row, col: houses[i].col}, 60);
                }
            }
            if (hasHouse === 0) {
                Animations({row: this._row, col: this._col}, {row: bdDest._row, col: bdDest._col}, 'dead', bdDest._id)
                VilBuildingArrRemover(bdDest);
                if (playerInfo[PlayerInfoSearch(bdDest._owner)].vils.length === 0) {
                    PlayerLoss(playerInfo[PlayerInfoSearch(bdDest._owner)]);
                }
                playerInfo[PlayerInfoSearch(bdDest._owner)].focus = noFocus;
//                playerInfo[PlayerInfoSearch(bdDest._owner)].focus = {};
//                playerInfo[PlayerInfoSearch(bdDest._owner)].focus._type = 'none';
                masterBoard[destination.row][destination.col].pop = {};
            }
        }
        if (this._type === 'guardtower') {
            BuildingRester({row: this._row, col: this._col}, 30);
        } else {
            Rester({row: this._row, col: this._col}, this._stats['_attack'])
        }
        PlayerChange(bdDest._owner)
        Animations({row: this._row, col: this._col}, destination, 'attack', 'none')
    }
    if (this._stats._range < dist) {
        MoveQ({row: bdDest._row, col: bdDest._col}, plArr.id, GridToCube({row: this._row, col: this._col}), GridToCube({row: bdDest._row, col: bdDest._col}))
    }
}

function Build(building) {
    if (stuffInfo[building].wood <= playerInfo[PlayerInfoSearch(this._owner)].res.wood && stuffInfo[building].stone <=
            playerInfo[PlayerInfoSearch(this._owner)].res.stone && masterBoard[this._row][this._col].building._id === undefined) {
        playerInfo[PlayerInfoSearch(this._owner)].res.wood -= stuffInfo[building].wood;
        playerInfo[PlayerInfoSearch(this._owner)].res.stone -= stuffInfo[building].stone;
        stuffSpawn = new NewStuff(building, this._row, this._col, this._owner, 0, IDMaker(), BdAppend, Focus, Move, [], {}, {}, Harvest,
                this._name, 0, 0, 0, Attack, Build, Upgrade, Trade, Demolish, 0, JSON.parse(JSON.stringify(stuffInfo[building])));
        masterBoard[this._row][this._col].building = stuffSpawn;
        playerInfo[PlayerInfoSearch(this._owner)].buildings.push(masterBoard[this._row][this._col].building);
        Animations({row: this._row, col: this._col}, 'none', 'move', stuffSpawn)
        if (building === 'house') {
            for (i = 0; i < houses.length; i++) {
                if (masterBoard[this._row][this._col].pop._id === houses[i].vil) {
                    TextHelp([0, 4], this._owner, '', {row: this._row, col: this._col}, this._id)
                    Animations({row: this._row, col: this._col}, {row: houses[i].row, col: houses[i].col},
                    'demolish', masterBoard[houses[i].row][houses[i].col].building._id)
                    masterBoard[houses[i].row][houses[i].col].building = {};
                    houses.splice(i, 1)
                }
            }
            houses.push({house: stuffSpawn._id, row: stuffSpawn._row, col: stuffSpawn._col, vil: this._id})
        }
    } else {
        TextHelp([0, 5], this._owner, this._name, {row: this._row, col: this._col}, this._id)
    }
}

function MoveQ(coords, plId, start, hex2) {
    var coordsSt = CubeToGrid(start)
    masterBoard[coordsSt.row][coordsSt.col].pop._moveQ = [];
    masterBoard[coordsSt.row][coordsSt.col].pop._secDest = {};
    var path = calculatePath(start, hex2, masterBoard, plId);
    masterBoard[coordsSt.row][coordsSt.col].pop._moveQ = path;
    if (masterBoard[coordsSt.row][coordsSt.col].pop._state === 4 &&
            masterBoard[coords.row][coords.col].building._owner === masterBoard[coordsSt.row][coordsSt.col].pop._owner) {
        path = [];
    }
    if (path.length > 0) {
        if (masterBoard[coordsSt.row][coordsSt.col].pop._active === 0) {
            MoveQMove(coordsSt);
        }
    } else {
        var paths = []
        for (i = 0; i < 6; i++) {
            var path = calculatePath(start, masterBoard[coords.row][coords.col].directions[i], masterBoard, plId)
            if (path.length > 0) {
                paths.push(path)
            }
        }
        if (paths.length > 0) {
            paths.sort(function (a, b) {
                return a.length - b.length
            });
            if (paths[0].length > 0) {
                masterBoard[coordsSt.row][coordsSt.col].pop._secDest = coords;
                masterBoard[coordsSt.row][coordsSt.col].pop._moveQ = paths[0];
                if (masterBoard[coordsSt.row][coordsSt.col].pop._active === 0) {
                    MoveQMove(coordsSt);
                }
            }
        }
    }
}

function MoveQMove(coords) {
    if (VacancyTest(CubeToGrid(masterBoard[coords.row][coords.col].pop._moveQ[0]),
            masterBoard[coords.row][coords.col].pop._owner) === true) {
        var coords2 = CubeToGrid(masterBoard[coords.row][coords.col].pop._moveQ[0]);
        masterBoard[coords.row][coords.col].pop._move(coords2);
        masterBoard[coords2.row][coords2.col].pop._moveQ.splice(0, 1);
        Rester(coords2, masterBoard[coords2.row][coords2.col].pop._stats['_move']);
    } else {
        if (masterBoard[coords.row][coords.col].pop._moveQ.length !== 1) {
            masterBoard[coords.row][coords.col].pop._secDest = {}
            MoveQ(coords, masterBoard[coords.row][coords.col].pop._owner,
                    GridToCube(coords), masterBoard[coords.row][coords.col].pop._moveQ[masterBoard[coords.row][coords.col].pop._moveQ.length - 1])
        } else {
            MoveQ(coords, masterBoard[coords.row][coords.col].pop._owner,
                    GridToCube(coords), GridToCube(masterBoard[coords.row][coords.col].pop._secDest))
        }
    }
}

function Rester(coords, time) {
    masterBoard[coords.row][coords.col].pop._active += time;
    PlayerChange(masterBoard[coords.row][coords.col].pop._owner)
    var startingID = masterBoard[coords.row][coords.col].pop._id;
    var interval = setInterval(function () {
        if (masterBoard[coords.row][coords.col].pop._id !== undefined &&
                masterBoard[coords.row][coords.col].pop._id === startingID) {
            masterBoard[coords.row][coords.col].pop._active -= 1;
            PlayerChange(masterBoard[coords.row][coords.col].pop._owner)
            if (masterBoard[coords.row][coords.col].pop._active <= 0) {
                clearInterval(interval)
                masterBoard[coords.row][coords.col].pop._active = 0;
                if (CubeDistance(GridToCube(masterBoard[coords.row][coords.col].pop._secDest),
                        masterBoard[coords.row][coords.col].cube) < 2 && masterBoard[coords.row][coords.col].pop._moveQ.length === 0) {
                    var coords5 = masterBoard[coords.row][coords.col].pop._secDest;
                    if (masterBoard[coords5.row][coords5.col].item !== 'none' &&
                            masterBoard[coords5.row][coords5.col].item !== 'block') {
                        masterBoard[coords.row][coords.col].pop._harvest(coords5)
                        masterBoard[coords.row][coords.col].pop._secDest = {};
                        Rester(coords, masterBoard[coords.row][coords.col].pop._stats['_harvest']);
                    }
                    if (masterBoard[coords5.row][coords5.col].building._id !== undefined &&
                            masterBoard[coords.row][coords.col].item !== 'chest' &&
                            (masterBoard[coords5.row][coords5.col].building._owner !== masterBoard[coords.row][coords.col].pop._id ||
                                    masterBoard[coords.row][coords.col].pop._state === 4)) {
                        masterBoard[coords.row][coords.col].pop._demolish(coords5)
                        masterBoard[coords.row][coords.col].pop._secDest = {};
                        Rester(coords, masterBoard[coords.row][coords.col].pop._stats['_demolish']);
                    }
                    masterBoard[coords.row][coords.col].pop._secDest = {}
                }
                if (masterBoard[coords.row][coords.col].pop._attTarget.vil === undefined) {
                    if (masterBoard[coords.row][coords.col].pop._moveQ.length > 0) {
                        MoveQMove(coords)
                    }
                } else {
                    var plin = PlayerInfoSearch(masterBoard[coords.row][coords.col].pop._attTarget.pl)
                    var vill = VillagerSearch(masterBoard[coords.row][coords.col].pop._attTarget.vil, plin)
                    if (playerInfo[plin].vils[vill] !== undefined) {
                        masterBoard[coords.row][coords.col].pop._attack({row: playerInfo[plin].vils[vill]._row, col: playerInfo[plin].vils[vill]._col},
                        CubeDistance(GridToCube({row: playerInfo[plin].vils[vill]._row, col: playerInfo[plin].vils[vill]._col}),
                                masterBoard[coords.row][coords.col].cube), playerInfo[PlayerInfoSearch(masterBoard[coords.row][coords.col].pop._owner)])
                    } else {
                        masterBoard[coords.row][coords.col].pop._attTarget = {};
                        masterBoard[coords.row][coords.col].pop._moveQ = [];
                    }
                }
            }
        } else {
            clearInterval(interval)
        }
    }, 1000)
}

function BuildingRester(coords, time) {
    masterBoard[coords.row][coords.col].building._active += time;
    PlayerChange(masterBoard[coords.row][coords.col].building._owner)
    var interval = setInterval(function () {
        masterBoard[coords.row][coords.col].building._active -= 1;
        PlayerChange(masterBoard[coords.row][coords.col].building._owner)
        if (masterBoard[coords.row][coords.col].building._active <= 0) {
            clearInterval(interval)
            masterBoard[coords.row][coords.col].building._active = 0;
        }
    }, 1000)
}

function Upgrade(upgrade) {
    var helpInt = 5;
    if (stuffInfo.upgrades[upgrade].wood <= playerInfo[PlayerInfoSearch(this._owner)].res.wood &&
            stuffInfo.upgrades[upgrade].sheep <= playerInfo[PlayerInfoSearch(this._owner)].res.sheep) {
        helpInt = 6;
        if (stuffInfo.upgrades[upgrade].limit !== this._stats['_' + upgrade]) {
            helpInt = 7;
            playerInfo[PlayerInfoSearch(this._owner)].res.wood -= stuffInfo.upgrades[upgrade].wood;
            playerInfo[PlayerInfoSearch(this._owner)].res.sheep -= stuffInfo.upgrades[upgrade].sheep;
            switch (upgrade) {
                case 'hpMax':
                    this._stats._hpMax += 3;
                    this._stats._hp += 3;
                    break;
                case 'attackDmg':
                    this._stats._attackDmg += 1;
                    break;
                case 'range':
                    this._stats._range += 1;
                    break;
                case 'harvest':
                    this._stats._demolish -= 2;
                    this._stats._harvest -= 1;
                    break;
                case 'move':
                    this._stats._move -= 1;
                    break;
                default:
            }
        }
    }
    TextHelp([0, helpInt], this._owner, this._name, {row: this._row, col: this._col}, this._id)
}

function Action(act, player) {
    var coords2 = {};
    var plArr = playerInfo[PlayerInfoSearch(player)];
    var hex = PixelToHex(act.pix.row, act.pix.col);
    var coords = CubeToGrid(hex);
    coords2.row = coords.row + plArr.view.row;
    coords2.col = coords.col + plArr.view.col;
    coords = OffEdgeOfGrid(coords2, boardSizes[totalUser].height, boardSizes[totalUser].width);
    var hex2 = GridToCube(coords);
    var start = GridToCube({row: plArr.focus._row, col: plArr.focus._col});
    var dist = CubeDistance(start, hex2)
    if (act.click === 'right' && plArr.focus._id !== undefined) {
        var tester = VillagerActionTest(coords, plArr, start, hex2, dist, plArr.focus._active);
        if (plArr.focus._active !== 0) {
            if (tester === 'attack') {
                plArr.focus._attack(coords, dist, plArr)
            }
            if (tester === 'move') {
                plArr.focus._attTarget = {};
                plArr.focus._secDest = {}
                MoveQ(coords, plArr.id, start, hex2);
            } else {
                plArr.focus._moveQ = [];
                plArr.focus._secDest = coords;
                tester = 'none'
            }
        }
        if (tester === 'attack' && plArr.focus._type === 'guardtower' && plArr.focus._active === 0) {
            plArr.focus._attack(coords, dist, plArr)
        }
        if (plArr.focus._type === 'trebuchet' && masterBoard[coords.row][coords.col].building._owner != undefined &&
                dist <= plArr.focus._stats._range && plArr.focus._active === 0) {
            BuildingRester({row: plArr.focus._row, col: plArr.focus._col}, 60)
            TextHelp([0, 8], masterBoard[coords.row][coords.col].building._owner, plArr.text.name, coords, masterBoard[coords.row][coords.col].building._id)
            Animations({row: plArr.focus._row, col: plArr.focus._col}, coords, 'demolish', masterBoard[coords.row][coords.col].building._id)
            if (masterBoard[coords.row][coords.col].building._type === 'house') {
                for (i = 0; i < houses.length; i++) {
                    if (houses[i].house === masterBoard[coords.row][coords.col].building._id) {
                        houses.splice(i, 1)
                    }
                }
            }
            plArr.focus._demolish(coords);
        }
        if (plArr.focus._type === 'vil') {
            switch (tester) {
                case "move":
                    plArr.focus._attTarget = {};
                    MoveQ(coords, plArr.id, start, hex2);
                    break;
                case "harvest":
                    plArr.focus._attTarget = {};
                    plArr.focus._harvest(coords)
                    Rester({row: plArr.focus._row, col: plArr.focus._col}, plArr.focus._stats['_' + tester])
                    break;
                case "attack":
                    plArr.focus._secDest = {};
                    plArr.focus._attack(coords, dist, plArr)
                    break;
                case "demolish":
                    plArr.focus._demolish(coords)
                    Rester({row: plArr.focus._row, col: plArr.focus._col}, plArr.focus._stats['_' + tester])
                    break;
                case "trade":
                    plArr.focus._trade(coords)
                    break;
                case "none":
                    break;
            }
        }
    }
    if (act.click === 'left') {
        if (masterBoard[coords.row][coords.col].building._owner === plArr.id) {
            masterBoard[coords.row][coords.col].building._focus(player)
            PlayerChange(player)
        }
        if (masterBoard[coords.row][coords.col].pop._owner === plArr.id) {
            masterBoard[coords.row][coords.col].pop._focus(player)
            PlayerChange(player)
        }
        if ((masterBoard[coords.row][coords.col].pop._owner !== undefined ||
                masterBoard[coords.row][coords.col].building._owner !== undefined) &&
                masterBoard[coords.row][coords.col].pop._owner !== plArr.id &&
                masterBoard[coords.row][coords.col].building._owner !== plArr.id) {
            var ownerID = ''
            if (masterBoard[coords.row][coords.col].building._owner !== undefined) {
                ownerID = masterBoard[coords.row][coords.col].building._owner
            }
            if (masterBoard[coords.row][coords.col].pop._owner !== undefined) {
                ownerID = masterBoard[coords.row][coords.col].pop._owner
            }
            TextHelp([0, 9], plArr.id, playerInfo[PlayerInfoSearch(ownerID)].text.name, coords, plArr.focus.id)
        }
    }
}

function VillagerActionTest(coords, plArr, start, hex2, dist, active) {
    if (dist === 1) {
        if (masterBoard[coords.row][coords.col].item !== 'none' &&
                masterBoard[coords.row][coords.col].item !== 'block') {
            return "harvest";
        }
        if (masterBoard[coords.row][coords.col].building._id !== undefined &&
                masterBoard[coords.row][coords.col].item !== 'chest' &&
                (masterBoard[coords.row][coords.col].building._owner !== plArr.id || plArr.focus._state === 4)) {
            return "demolish";
        }
        if (masterBoard[coords.row][coords.col].pop._id !== undefined &&
                plArr.focus._state === 3) {
            return "trade";
        }
    }
    if (masterBoard[coords.row][coords.col].pop._owner !== undefined &&
            masterBoard[plArr.focus._row][plArr.focus._col].pop._owner !==
            masterBoard[coords.row][coords.col].pop._owner) {
        return "attack";
    }
    if (dist > 0) {
        return "move";
    }
    return 'none'
}

function VacancyTest(coords, owner) {
    if (masterBoard[coords.row][coords.col].pop._owner === undefined &&
            masterBoard[coords.row][coords.col].item === 'none' &&
            (masterBoard[coords.row][coords.col].building._owner === undefined ||
                    masterBoard[coords.row][coords.col].building._owner === owner)) {
        return true;
    }
    if (masterBoard[coords.row][coords.col].item !== 'none' &&
            masterBoard[coords.row][coords.col].item !== 'block') {
        return masterBoard[coords.row][coords.col].item;
    }
    return false;
}

var IDMaker = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

function Range(hex, range) {
    var results = [];
    for (k = -range; k <= range; k++) {
        for (l = -range; l <= range; l++) {
            for (m = -range; m <= range; m++) {
                if (k + l + m === 0) {
                    var res = CubeToGrid({x: hex.x + k, z: hex.z + l, y: hex.y + m});
                    res = OffEdgeOfGrid(res, boardSizes[totalUser].height, boardSizes[totalUser].width);
                    results.push(GridToCube(res));
                }
            }
        }
    }
    return results;
}



function BuildingCounter() {
    var counter = 0;
    for (i = 0; i < playerInfo.length; i++) {
        if (playerInfo[i].buildings.length > counter) {
            mostBuildings = i;
            counter = i;
        }
    }
}

function VillagerCounter() {
    var counter = 0;
    for (i = 0; i < playerInfo.length; i++) {
        if (playerInfo[i].vils.length > counter) {
            mostVillagers = i;
            counter = i;
        }
    }
}

function OldestCounter() {
    var counter = 0;
    for (i = 0; i < playerInfo.length; i++) {
        if (playerInfo[i].vils.length > 0) {
            mostOldest = i;
            counter = i;
            break;
        }
    }
}

function HighScoreAssembler() {
    setInterval(function () {
        BuildingCounter();
        VillagerCounter();
        OldestCounter();
    }, 5000)
}

function PlayerLoss(playerInf) {
    var arr = playerInf
    TextHelp([0, 14], 'all', arr.text.name)
    for (g = 0; g < arr.vils.length; g++) {
        Animations({row: arr.vils[g]._row, col: arr.vils[g]._col * 2}, {row: arr.vils[g]._row, col: arr.vils[g]._col}, 'dead',
                masterBoard[arr.vils[g]._row][arr.vils[g]._col].pop._id)
        masterBoard[arr.vils[g]._row][arr.vils[g]._col].pop = {};
        arr.vils = {};
    }
    for (g = 0; g < arr.buildings.length; g++) {
        Animations({row: arr.buildings[g]._row, col: arr.buildings[g]._col * 2}, {row: arr.buildings[g]._row, col: arr.buildings[g]._col}, 'demolish',
                masterBoard[arr.buildings[g]._row][arr.buildings[g]._col].building._id)
        masterBoard[arr.buildings[g]._row][arr.buildings[g]._col].building = {};
        arr.buildings = {};
    }
    --userCount;
}

Start();

function PlayerChange(pl) {
    if (io.sockets.connected[pl] !== undefined) {
        io.sockets.connected[pl].emit('player', playerInfo[PlayerInfoSearch(pl)]);
        io.emit('servInfo', {info: {users: userCount, size: boardSizes[totalUser].area, mostBuildings: playerInfo[mostBuildings].text.name,
                mostVillagers: playerInfo[mostVillagers].text.name, oldest: playerInfo[mostOldest].text.name}, colors: 'none'});
    }
}

function Animations(a, b, type, id) {
    io.emit('animations', {id: id, a: a, b: b, type: type});
}

function TextHelp(msg, pl, textra, coords, id) {
    if (pl === 'all') {
        io.emit('animations', {msg: msg, textra: textra, coords: coords, id: id, type: 'help'})
    } else {
        io.sockets.connected[pl].emit('animations', {msg: msg, textra: textra, coords: coords, id: id, type: 'help'})
    }
}

io.on('connection', function (socket) {
    ++userCount;
    ++totalUser;
    io.sockets.connected[socket.id].emit('initial', stuffInfo);
    masterBoard = BoardReplace(totalUser);
    AddPlayer(socket.id);
    if (kickstart === 0) {
        HighScoreAssembler()
        kickstart = 1;
        SheepMover()
    }
    socket.on('action', function (action) {
        Action(action, socket.id);
    });
    socket.on('animations', function (action) {

    });
    socket.on('compass', function (axis) {
        var plInt = PlayerInfoSearch(socket.id);
        var view = Compass(axis, plInt);
        playerInfo[plInt].view.row = view.row;
        playerInfo[plInt].view.col = view.col;
        PlayerChange(socket.id);
        io.sockets.connected[socket.id].emit('map', {view: playerInfo[plInt].view, board: masterBoard});
    });
    socket.on('servInfo', function (axis) {

    });
    socket.on('plInfo', function (data) {
        var plInt = PlayerInfoSearch(socket.id);
        if (data.val !== 'off') {
            switch (n = data.name) {
                case 'vil':
                    playerInfo[plInt].focus._name = data.val
                    for (i = 0; i < houses.length; i++) {
                        if (houses[i].vil === playerInfo[plInt].focus._id) {
                            masterBoard[houses[i].row][houses[i].col].building._name = data.val;
                        }
                    }
                    PlayerChange(socket.id)
                    break;
                case 'chat':
                    Animations({row: playerInfo[plInt].focus._row, col: playerInfo[plInt].focus._col}, 'none', 'chat',
                            {vil: playerInfo[plInt].focus._name, village: playerInfo[plInt].text.name, chat: data.val, id: playerInfo[plInt].focus._id})
                    playerInfo[plInt].focus._chat = data.val
                    PlayerChange(socket.id)
                    break;
                case 'build':
                    playerInfo[plInt].focus._build(data.val)
                    break;
                case 'upgrades':
                    playerInfo[plInt].focus._upgrade(data.val)
                    break;
                case 'map':
                    playerInfo[plInt].focus = playerInfo[plInt].vils[VillagerSearch(data.val.id, plInt)];
                    if (playerInfo[plInt].focus === undefined) {
                        playerInfo[plInt].focus = noFocus;
                    }
                    var viewAd = OffEdgeOfGrid({row: 2 * Math.round((playerInfo[plInt].focus._row - playerInfo[plInt].viewSize.height / 2) / 2),
                        col: 2 * Math.round((playerInfo[plInt].focus._col - playerInfo[plInt].viewSize.width / 2) / 2)},
                    boardSizes[totalUser].height, boardSizes[totalUser].width);
                    playerInfo[plInt].view.row = viewAd.row;
                    playerInfo[plInt].view.col = viewAd.col;
                    io.sockets.connected[socket.id].emit('map', {view: playerInfo[plInt].view, board: masterBoard});
                    PlayerChange(socket.id);
                    break;
                case 'map2':
                    var viewAd = OffEdgeOfGrid({row: 2 * Math.round((data.val.coords.row - playerInfo[plInt].viewSize.height / 2) / 2),
                        col: 2 * Math.round((data.val.coords.col - playerInfo[plInt].viewSize.width / 2) / 2)},
                    boardSizes[totalUser].height, boardSizes[totalUser].width);
                    playerInfo[plInt].view.row = viewAd.row;
                    playerInfo[plInt].view.col = viewAd.col;
                    io.sockets.connected[socket.id].emit('map', {view: playerInfo[plInt].view, board: masterBoard});
                    PlayerChange(socket.id);
                    break;
                case 'trade':
                    playerInfo[plInt].focus._trade(data.val)
                    break;
                case 'demolish':
                    playerInfo[plInt].focus._state = 4;
                    break;
                case 'resize':
                    playerInfo[plInt].viewSize.height = Math.min(data.val.newSize.height, boardSizes[totalUser].height);
                    playerInfo[plInt].viewSize.width = Math.min(data.val.newSize.width, boardSizes[totalUser].width);
                    PlayerChange(socket.id);
                    io.sockets.connected[socket.id].emit('board', masterBoard);
                    break;
                case 'setup':
                    playerInfo[plInt].text.name = data.val.name;
                    playerInfo[plInt].viewSize.height = Math.min(data.val.viewSize.height, boardSizes[totalUser].height);
                    playerInfo[plInt].viewSize.width = Math.min(data.val.viewSize.width, boardSizes[totalUser].width);
                    stuffInfo.colors.users[plInt].colors[0] = data.val.color1;
                    stuffInfo.colors.users[plInt].colors[1] = data.val.color2 - 20;
                    io.emit('servInfo', {info: {users: userCount, size: boardSizes[totalUser].area},
                        colors: stuffInfo.colors.users});
                    PlayerJoin(plInt)
                    PlayerChange(socket.id);
                    io.sockets.connected[socket.id].emit('board', masterBoard);
                    break;
                default:
                    break;
            }
        } else {
            playerInfo[plInt].focus._state = 0
        }
    }
    );
    socket.on('disconnect', function () {
        PlayerLoss(playerInfo[PlayerInfoSearch(socket.id)])
    });
});

function Compass(axis, pl) {
    var newView = {row: playerInfo[pl].view.row, col: playerInfo[pl].view.col};
    switch (axis.axis) {
        case "n":
            newView.row = playerInfo[pl].view.row - 4
            break;
        case "w":
            newView.col = playerInfo[pl].view.col - 4
            break;
        case "e":
            newView.col = playerInfo[pl].view.col + 4
            break;
        case "s":
            newView.row = playerInfo[pl].view.row + 4
            break;
        default:
    }
    newView = OffEdgeOfGrid(newView, boardSizes[totalUser].height, boardSizes[totalUser].width);
    return newView
}


function calculatePath(pathStart, pathEnd, board, owner) {
// create Nodes from th Start and End x,y coordinates
    var nodes = [];
    var mypathStart = Node(null, {x: pathStart.x, z: pathStart.z, y: pathStart.y});
    var mypathEnd = Node(null, {x: pathEnd.x, z: pathEnd.z, y: pathEnd.y});
    // create an array that will contain all world cells
    var AStar = new Array(768);
    // list of currently open Nodes
    var Open = [mypathStart];
    // list of closed Nodes
    var Closed = [];
    // list of the final output array
    var result = [];
    // reference to a Node (that is nearby)
    var myNeighbours = [];
    // reference to a Node (that we are considering now)
    var myNode;
    // reference to a Node (that starts a path in question)
    var myPath;
    // temp integer variables used in the calculations
    var length, max, min, i, j;
    // iterate through the open list until none are left
    var gridNode;
    while (length = Open.length)
    {
        max = 768;
        min = -1;
        for (i = 0; i < length; i++)
        {
            if (Open[i].f < max)
            {
                max = Open[i].f;
                min = i;
            }
        }
// grab the next node and remove it from Open array
        myNode = Open.splice(min, 1)[0];
        // is it the destination node?
        if (myNode.value === mypathEnd.value)
        {
            myPath = Closed[Closed.push(myNode) - 1];
            do
            {
                result.push({x: myPath.x, z: myPath.z, y: myPath.y});
            }
            while (myPath = myPath.Parent);
            // clear the working arrays
            AStar = Closed = Open = [];
            // we want to return start to finish
            result.reverse();
        }
        else // not the destination
        {
// find which nearby nodes are walkable
            gridNode = OffEdgeOfGrid(CubeToGrid({x: myNode.x, z: myNode.z, y: myNode.y}),
                    boardSizes[totalUser].height, boardSizes[totalUser].width);
            var myNeighbours = JSON.parse(JSON.stringify(board[gridNode.row][gridNode.col].directions));
//            myNeighbours = board[gridNode.row][gridNode.col].directions;
            for (i = 0, j = myNeighbours.length; i < j; i++) {
                if (VacancyTest(CubeToGrid(myNeighbours[j - i - 1]), owner) !== true) {
                    myNeighbours.splice(j - i - 1, 1);
                }
            }
            // test each one that hasn't been tried already
            for (i = 0, j = myNeighbours.length; i < j; i++) {
                myPath = Node(myNode, myNeighbours[i]);
                if (!AStar[myPath.value])
                {
                    // estimated cost of this particular route so far
                    myPath.g = myNode.g + CubeDistance(myNeighbours[i], myNode);
                    // estimated cost of entire guessed route to the destination
                    myPath.f = myPath.g + CubeDistance(myNeighbours[i], mypathEnd);
//                    myPath.h = myPath.g + myPath.f + CubeDistance(myNeighbours[i], mypathEnd);
                    // remember this new path for testing above
                    Open.push(myPath);
                    AStar[myPath.value] = true;
                }
            }
// remember this route as having no more untested options
            Closed.push(myNode);
        }
    } // keep iterating until until the Open list is empty    
    result.splice(0, 1)
    return result;
}


function Node(Parent, Point)
{
    var newNode = {
        // pointer to another Node object
        Parent: Parent,
        // array index of this Node in the world linear array
        value: Point.x + (Point.y * boardSizes[totalUser].width),
        // the location coordinates of this Node
        x: Point.x,
        z: Point.z,
        y: Point.y,
        // the distanceFunction cost to get
        // TO this Node from the START
        f: 0,
        // the distanceFunction cost to get
        // from this Node to the GOAL
        g: 0,
        h: 0
    };
    return newNode;
}