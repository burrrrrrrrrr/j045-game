var size = 27.75;

function HexToCube(h) {
    var he = {};
    he.x = h.q;
    he.z = h.r;
    he.y = -he.x - he.z;
    var hexInt = CubeRound(he);
    return hexInt;
}

function PixelToHex(x, y) {
    var h = {};
    h.q = x * 2 / 3 / size;
    h.r = (-x / 3 + Math.sqrt(3) / 3 * y) / size;
    var hex = HexToCube(h);
    return hex;
}

function HexToPixel(hex) {
    var x = size * 3/2 * hex.x
    var y = size * Math.sqrt(3) * (hex.z + hex.x/2)
    return {x:x, y:y};
}

function HexRound(h) {
    var CubeToHex = CubeRound(HexToCube(h));
    return CubeToHex
}


function CubeRound(h) {
    var rx = Math.round(h.x);
    var ry = Math.round(h.y);
    var rz = Math.round(h.z);

    var x_diff = Math.abs(rx - h.x);
    var y_diff = Math.abs(ry - h.y);
    var z_diff = Math.abs(rz - h.z);

    if (x_diff > y_diff && x_diff > z_diff) {
        rx = -ry - rz;
    } else if (y_diff > z_diff) {
        ry = -rx - rz;
    } else {
        rz = -rx - ry;
    }
    ;
    return {x: rx, z: rz, y: ry};
}

function GridToCube(gridcoords) {
    var cube = {};
    cube.x = gridcoords.col;
    cube.z = gridcoords.row - (gridcoords.col - (gridcoords.col & 1)) / 2;
    cube.y = -cube.x - cube.z;
    return cube;
}

function CubeToGrid(cube) {
    var col = cube.x;
    var row = cube.z + (cube.x - (cube.x & 1)) / 2;
    return {row: row, col: col};
}